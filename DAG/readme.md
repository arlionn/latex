
&emsp;

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876) 

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhtop00.png)](https://www.lianxh.cn/news/46917f1076104.html)

> **温馨提示：** 定期 [清理浏览器缓存](http://www.xitongzhijia.net/xtjc/20170510/97450.html)，可以获得最佳浏览体验。

> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索推文、Stata 资源。安装：  
> &emsp; `. ssc install lianxh`  
> 详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`    
> 连享会新命令：`cnssc`, `ihelp`, `rdbalance`, `gitee`, `installpkg`      
&emsp;


> &#x270C; **[课程详情](https://www.lianxh.cn/news/46917f1076104.html)：** <https://gitee.com/lianxh/Course> 

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhtop01.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

> &#x26F3; **[课程主页](https://www.lianxh.cn/news/46917f1076104.html)：** <https://gitee.com/lianxh/Course>

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhtop02.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

> &#x26F3; Stata 系列推文：

- [全部](https://www.lianxh.cn/blogs.html)  | [Stata入门](https://www.lianxh.cn/blogs/16.html) |  [Stata教程](https://www.lianxh.cn/blogs/17.html) |  [Stata资源](https://www.lianxh.cn/blogs/35.html) | [Stata命令](https://www.lianxh.cn/blogs/43.html) 
- [计量专题](https://www.lianxh.cn/blogs/18.html) | [论文写作](https://www.lianxh.cn/blogs/31.html) | [数据分享](https://www.lianxh.cn/blogs/34.html) |  [专题课程](https://www.lianxh.cn/blogs/44.html)
- [结果输出](https://www.lianxh.cn/blogs/22.html) | [Stata绘图](https://www.lianxh.cn/blogs/24.html) | [数据处理](https://www.lianxh.cn/blogs/25.html) |  [Stata程序](https://www.lianxh.cn/blogs/26.html)
- [回归分析](https://www.lianxh.cn/blogs/32.html) |  [面板数据](https://www.lianxh.cn/blogs/20.html)  | [交乘项-调节](https://www.lianxh.cn/blogs/21.html)  | [IV-GMM](https://www.lianxh.cn/blogs/38.html) 
- [内生性-因果推断](https://www.lianxh.cn/blogs/19.html) |  [倍分法DID](https://www.lianxh.cn/blogs/39.html) |  [断点回归RDD](https://www.lianxh.cn/blogs/40.html) |  [PSM-Matching](https://www.lianxh.cn/blogs/41.html) |  [合成控制法](https://www.lianxh.cn/blogs/42.html)
- [Probit-Logit](https://www.lianxh.cn/blogs/27.html) |  [时间序列](https://www.lianxh.cn/blogs/28.html) |  [空间计量](https://www.lianxh.cn/blogs/29.html) | [分位数回归](https://www.lianxh.cn/blogs/48.html) | [生存分析](https://www.lianxh.cn/blogs/46.html) | [SFA-DEA](https://www.lianxh.cn/blogs/49.html)
- [文本分析-爬虫](https://www.lianxh.cn/blogs/36.html) |  [Python-R-Matlab](https://www.lianxh.cn/blogs/37.html) | [机器学习](https://www.lianxh.cn/blogs/47.html)
- [Markdown](https://www.lianxh.cn/blogs/30.html)  | [工具软件](https://www.lianxh.cn/blogs/23.html) |  [其它](https://www.lianxh.cn/blogs/33.html)


> &#x261D; [PDF下载 - 推文合集](https://www.jianguoyun.com/p/DXgO9zoQtKiFCBj6tPED )

&emsp;


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

> **作者：** 左祥太 (武汉纺织大学)  
> **邮箱：** <Shutter_Z@outlook.com>

&emsp;

&emsp;

---

**目录**
[[TOC]]

---

&emsp;

## 特别说明

本文主要来自 Eleanor Murray 博士建立的 [Github - causalgraphs_latex 仓库](https://github.com/eleanormurray/causalgraphs_latex)，特此致谢。

在 [中文镜像仓库](https://gitee.com/arlionn/causalgraphs_latex)) 中，我们放置了作者提供的 PDF 原文和 LaTeX 源代码，大家可以前往下载，以便尽快熟悉使用 LaTeX 绘制因果图、决策树、概率图、有向无环图，以及结构方程图形的语法。

本文是 [连享会 - LaTeX 仓库](https://gitee.com/arlionn/latex) 的一部分，与之对应的姊妹仓库是 [五分钟 Markdown](https://gitee.com/arlionn/md)。

本文介绍如何使用 LaTeX 绘制常见的有向无环图 (DAG)：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817151355659.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817154327572.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817160056330.png)

&emsp;

## 1. LaTeX 介绍

**LaTex** 是一种基于 **TEX** 的排版系统，由美国计算机学家 _Leslie Lamport_ 在 20 世纪 80 年代初期开发，利用这种格式，可以使使用者在短时间内生成满足各种期刊投稿要求格式的文章，充分地解决了部分情况下 **Word** 排版困难的问题。

当然，若在本地使用 LaTeX 编写文档，需要安装一些软件，做一些基本配置，事前的准备工作比使用 Word 和 Markdown 写东西要复杂一些。详情参见第 1.1 小节。

若只是想感受一下 LaTeX，然后再决定去留，可以考虑在线编译网站 <https://www.overleaf.com/>。只需花 5 秒钟填入邮箱，注册后即可开始编写 TeX 文档，并在线编译输入 PDF 文档。本文后续给出的范例都可以直接贴入该网站进行编译和修改。详情参见第 1.2 小节。

相关介绍如下：

- [LaTeX 新手入门以及 TeXlive 和 TeXstudio 的安装使用](https://blog.csdn.net/zywhehe/article/details/83113214)
- [如何使用 TeX Live 和 TeXstudio 操作 LaTeX？](https://www.zhihu.com/question/25696183)
- [TeX Live & TeXstudio 安装手记](https://www.cnblogs.com/joyeecheung/p/3596255.html)

### 1.1 本地编译：安装 TeXLive 套装和 TeXstudio 编辑器

目前比较主流的做法是：

- **(1) 编译器。** 安装 TeXlive 套装来编译 .tex 文档，虽然该套装也自带了编辑器的，但功能相对单一。安装事宜参见如下文档：
  - **下载：** [清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/)
  - LaTeX 简介及安装说明 1：[LaTeX 小白入门：TeXLive 安装及使用](https://www.lianxh.cn/news/bb253cd035ef4.html)
  - 安装和配置 TeX Live：[最新 TeXLive 环境的安装与配置](https://zhuanlan.zhihu.com/p/41855480)
  - [TexLive 中文文档](http://www.tug.org/texlive/doc/texlive-zh-cn/texlive-zh-cn.pdf)
- **(2) 编辑器。** 使用 TeXstudio 作为编辑器，它提供了很多便捷功能，比如自动补全，语法高亮，反向链接等，可以大幅提高编辑 .tex 的速度。
  - **安装：** [TexStudio 官网](http://texstudio.sourceforge.net/)，[下载和安装说明]()
  - 点击左侧菜单列表中的「Download」按钮 &rarr; 「**[download](https://github.com/texstudio-org/texstudio/releases/download/3.1.2/texstudio-3.1.2-win-portable-qt5.zip) and unzip the zip**」
  - 中文支持相关设定
    - **字符编码**：点击界面右下角底部第二个菜单，选择「**UTF-8**」。这是支持中文的关键。
    - **编译器**：依次点击 **Options** &rarr; **Configure TeXstudio** &rarr; **Build** &rarr; **Default Compiler** &rarr; `XeLaTeX`
    - TexStudio 编译中文模板时会出现各种报错，大多数是因为字体缺失，将这些字体复制到 **C:/Windows/fonts** 文件夹下可以解决上述问题。
    - 编译不通过，有时候是加载了太多宏包，彼此冲突，可以关掉一些宏包；有时则需要调整宏包的加载顺序，比如，`Hyperref` 最好放在最后

### 1.2 在线编译

考虑到下载速度等外部因素的影响，本文也提供一种在线的 **LaTeX** 编辑器使用方式，后续 **画图** 部分的实操将通过 **overleaf** 实现：[Overleaf, Online LaTeX Editor](https://www.overleaf.com/login) 。
相关文档如下：

- [Learn LaTeX in 30 minutes](https://cn.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes)
- [神器 Overleaf！](https://cloud.tencent.com/developer/article/1425310)

大家可以通过各种方式登录，并新建一个临时项目，从而完成简易的绘图工作。具体步骤如下：

> - 登陆 **LaTeX 在线编译网址**，并注册：<https://www.overleaf.com/login>  
>   &emsp;
> - 新建一个项目，贴入本推文后续介绍的范例代码，单击「重新编译」  
>   &emsp;
> - 作为练习，可以自行修改左侧的代码，然后「重新编译」，对比效果变化

具体界面如下：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20210908143839.png)

&emsp;

## 2. 利用 LaTex 绘制有向无环图 DAGs

有向无环图 (DAG) 是评估变量之间的假设关系的工具，即这些变量在给定的研究或分析中造成偏差的可能性。它们是设计研究或分析、评估意外结果的潜在原因、或讲解因果推断的有用工具。在因果推断中常用的 DAG 有两种类型:

- 统计学 DAG 只包含测量的或已知的变量;
- 因果 DAG 包含所有感兴趣的变量与更多感兴趣的变量的所有共同潜在变量，即使这些变量的身份未知。

### 2.1 导入宏包

宏包之于 **Texstudio** 就如同命令之于 **Stata** 一般，与 **Stata** 执行操作时需要调用对用的命令一般，想要在 **Texstudio** 中实现一定的功能，就需要在 **.tex** 文档最前方调用对应的宏包。

```latex
% 在begin{document}之前的部分都可以称作“导言区”
% 导言区用以对宏包进行调用以及设置页面基本信息
% 方便起见这里一次性调用作者提及的所有宏包
\documentclass[a4paper]{report}
\usepackage[UTF8]{ctex}
% 调用UTF8以正确显示中文

\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}
% 绘图的主要宏包

\usepackage{hyperref}
% 至此调用完所有的宏包，编译发现页面仍空白（因为还没开始写正文）
```

### 2.2 设置标题作者等信息

```latex
% 设置超链接格式（可以不设置）
\hypersetup{
	colorlinks=true,
	linkcolor=black,
	filecolor=black,
	urlcolor=blue,
}

% 设置脚注等（非必要）
\pagestyle{fancy}
\fancyhf{}
\lfoot{连享会}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0pt}

% 设置标题作者等
\title{使用LaTex绘制有向无环图 (DAG)}
\author{译制：左祥太(武汉纺织大学)\\原作：Eleanor Murray (\url{github.com/eleanormurray}) }
% 如需在文中展示作者信息则需要在 \begin{document}下方加入 \maketitle
```

### 2.3 开始绘制图片

**注意：** 在 **LaTex** 当中，`begin` 与 `end` 一定要成对出现，不然会报错。后续绘图直接在 **overleaf** 网页的新建项目当中通过复制粘贴与编译即可得到对应图片。

&emsp;

## 3. 统计学 DAG

统计学 DAG 用于展示有关于已知变量之间关联的概念。在统计 DAG 中，箭头意味着潜在的关联，可能是也可能不是因果关系。在统计 DAG 中，任何没有被箭头连接的变量都是已知或假定没有关联的。

统计 DAG 可以使用机器学习或其他统计技术从数据中收集并建立，但不应该被用来确定两个变量是否有因果关系。

### 3.1 有混杂因素的统计 DAG

在下一个 DAG 中，我们增加了一个已知的混杂因素，$L$ --即一个与暴露和结果相关的变量，并且在时间上先于两者发生，因此它可以解释暴露和结果之间检测到的任何虚假（非因果）关联。$L$ 也可以代表一组与 $A$ 和 $Y$ 有类似关系的混杂变量。

> &#x2B55; 温馨提示：将如下范例 TeX 文档贴入 [Overleaf, Online LaTeX Editor](https://www.overleaf.com/login)，即可在线编辑。  
> <https://www.overleaf.com/login>

```latex
\documentclass{article}
% 文章类型文档声明
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}
% 导入主要所需宏包

\begin{document}
% 开始文档内容书写

\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]
% 设置箭头格式类型

\begin{tikzpicture}
\node  (2) {L};
\node [right =of 2] (3) {A};
\node [right =of 3] (4) {Y};

\draw[Arrow] (2.east) -- (3.west);
\draw[Arrow] (2) to [out=25, in=160] (4);
\end{tikzpicture}
% 正式画图

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817151355659.png)

### 3.2 对一个混杂因素进行限制的统计 DAG

在这个 DAG 中，$L$ 周围有一个方框，表示变量 $L$ 已被限制在某一组数值内，或者在分析中已被统计调整。下面的 DAG 是在假设无效假设为真的情况下绘制的。代码中唯一不同的是，我们在第一个节点周围画了一个矩形。然而，DAG 现在编码的假设是，在任何以 $L$ 为条件或限制的分析中，暴露和结果都不会有关联。

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node [rectangle, draw] (1) {L};
\node [right =of 1] (2) {A};
\node [right =of 2] (3) {Y};

\draw[Arrow, thick] (1.east) -- (2.west);
\draw[Arrow, thick] (1) to [out=25, in=160] (3);
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817151725700.png)

### 3.3 对碰撞点进行限制的统计 DAG

在这个 DAG 中，有一个额外的变量 $C$ ，用于审查或失去跟踪。由于我们通常只有留在研究中的个体的数据，对于我们有数据的所有个体来说，$C$ 等于 0（未删减），因此根据定义是受限制的。因此，在任何有删减的 DAG 中，C 周围应该有一个方框来表示这种限制。

请注意，当我们以暴露的下游后果为条件时，我们就会产生碰撞点偏差的可能性。因此，从我们以 $C$ 为条件或限制的数据中创建的统计 DAG 很可能会检测到暴露和结果之间的关联。这种可能性在统计 DAG 中由 $A$ 和 $Y$ 之间的一个箭头表示。如果我们省略了从 $A$ 到 $Y$ 的这个箭头，我们就是在做一个非信息删减的假设。这相当于假设任何有缺失结果信息的观察值是完全随机缺失的（MCAR）。

> &#x2B55; 温馨提示：将如下范例 TeX 文档贴入 [Overleaf, Online LaTeX Editor](https://www.overleaf.com/login)，即可在线编辑。  
> <https://www.overleaf.com/login>

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node (1) {A};
\node [rectangle, draw, right =of 1] (2) {C};
\node [right =of 2] (3) {Y};

\draw[Arrow, thick] (1) to [out=25, in=160] (3);
\draw[Arrow, thick] (1.east) -- (2.west);
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817153656336.png)

### 3.4 持续暴露的统计学 DAG

当数据是纵向的并且在多个时间点上可用时，DAG 必须在每个时间点上为每个变量设置单独的节点，以便保持非周期性并避免循环，时间点可以用下标来表示。

这里有一个随时间变化的混杂因素 $L$ ，它也受先前治疗的影响。如果分析不对所有时间点的 $L$ 进行调整，就会出现因混杂而产生的偏差。然而，如果分析通过简单的基于分层的回归分析在所有时间点上对 $L$ 进行调整，则可能会因在 $L_k$ 的碰撞点上进行调节而出现偏差。

下面的 DAG 是一个完整的 DAG ，也就是说，除了直接从每个处理方法 $A$ 到结果 $Y$ 的箭头外，所有可能的箭头都存在。然而，如果存在混杂因素 $L_k$ 和结果的任何共同原因，那么任何试图仅从数据中创建这个统计 DAG 的做法都将导致在所有时间点上观察到 $A$ 和 $Y$ 之间的关联。

为简单起见，在下面的 DAG 中省略了 $Y_k$ ，$Y_{k+1}$ 可以被认为是随访结束时结果的累积概率（发病率）的指标。

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node (1) {L$_{k-1}$};
\node [right =of 1] (2) {A$_{k-1}$};
\node [right =of 2] (3) {L$_{k}$};
\node [right =of 3] (4) {A$_{k}$};
\node [right =of 4] (5) {Y$_{k+1}$};

\draw[Arrow, thick] (1.east) -- (2.west);
\draw[Arrow, thick] (1) to [out=25, in=160] (3);
\draw[Arrow, thick] (1) to [out=25, in=160] (4);
\draw[Arrow, thick] (1) to [out=25, in=160] (5);

\draw[Arrow, thick] (2.east) -- (3.west);
\draw[Arrow, thick] (2) to [out=25, in=160] (4);

\draw[Arrow, thick] (3.east) -- (4.west);
\draw[Arrow, thick] (3) to [out=25, in=160] (5);
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817154014901.png)

&emsp;

## 4. 因果 DAG

因果 DAG 编码了对相关变量之间因果关系的图像。在因果 DAG 中，变量之间的箭头意味着潜在的因果关系。在因果 DAG 中，任何没有被箭头连接的变量都是已知的或假定的缺乏因果关系，当两个变量已知有关联但不被认为有因果关系时，就用一个未知变量 $U$ 来表示。

### 4.1 有混杂因素的因果 DAG

> &#x2B55; 温馨提示：将如下范例 TeX 文档贴入 [Overleaf, Online LaTeX Editor](https://www.overleaf.com/login)，即可在线编辑。  
> <https://www.overleaf.com/login>

任何因果 DAG 必须包括 DAG 上任何两个变量的所有可能的共同原因。在最简单的情况下，所有的混杂物都是已知的，混杂物的统计 DAG 将与混杂物的因果 DAG 相同。在更复杂的情况下，混杂的因果 DAG 可以包括 $A$ 和 $Y$ 、$L$ 和 $A$ 以及 $L$ 和 $Y$ 的其他未知共同原因。

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node  (2) {L};
\node [right =of 2] (3) {A};
\node [right =of 3] (4) {Y};
\node [left =of 2] (5) {U$_1$};
\node [below =of 5] (6) {U$_2$};
\node [below =of 2] (7) {U$_3$};

\draw[Arrow] (2.east) -- (3.west);
\draw[Arrow] (2) to [out=25, in=160] (4);

\draw[Arrow] (5.east) -- (2.west);
\draw[Arrow] (5) to [out=25, in=160] (4);

\draw[Arrow] (6.north) -- (2.south);
\draw[Arrow] (6.north) -- (3.south);

\draw[Arrow] (7.north) -- (3.south);
\draw[Arrow] (7.north) -- (4.south);
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817154327572.png)

### 4.2 对碰撞点进行限制的因果 DAG

如同有对撞点的统计 DAG ，因果 DAG 有一个额外的变量 $C$，用于删减或失去跟踪的值。由于我们通常只有留在研究中的个体的数据，对于我们有数据的所有个体来说，$C$ 等于 0（未删减），因此根据定义是受限制的。因此，在任何有删减的 DAG 中，$C$ 周围应该有一个方框来表示这种限制。

与统计 DAG 不同，因果 DAG 还包括 $C$ 和 $Y$ 的未知共同原因。大多数有删减或随访损失的研究可能有一些已知或未知的删减和结果的共同原因。从因果 DAG 中省略 $U$（或其他一些已知的共同原因），就会产生非信息性删减或结果信息完全随机缺失的假设（MCAR）。通过包括 $U$，我们放松了这个（强）假设，而允许删减是有信息的。如果我们能够识别和测量 $U$ ，那么我们就可以对删减过程进行调整，现在只假设结果是随机缺失的（MAR），或者删减是有条件的非信息性的。

请注意，由于 $C$ 是 $A$ 和 $U$ 的共同效应，所以 $C$ 是一个碰撞器。因此，不考虑 $U$ 的数据分析将导致 $A$ 和 $Y$ 之间通过 $A\rightarrow C \leftarrow U \rightarrow L$ 的路径产生人为的非因果关联，而这一路径是通过限制碰撞点 C 而打开的。

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node (1) {A};
\node [rectangle, draw, right =of 1] (2) {C};
\node [right =of 2] (3) {Y};
\node [below =of 1] (4) {U};

\draw[Arrow, thick] (1.east) -- (2.west);
\draw[Arrow, thick] (4.east) -- (2.west);
\draw[Arrow, thick] (4.east) -- (3.west);
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817160056330.png)

### 4.3 持续暴露的因果 DAG

当数据是纵向的并且在多个时间点上可用时，DAG 必须在每个时间点上为每个变量设置单独的节点，以便保持非周期性并避免循环。时间点可以用下标来表示。

这里有一个随时间变化的混杂因素，$L$，它也受先前治疗的影响。如果分析不对所有时间点的 $L$ 进行调整，就会出现因混杂而产生的偏差。然而，如果分析通过简单的基于分层的回归分析在所有时间点上对 $L$ 进行调整，则可能会因在 $L_k$ 的碰撞点上的条件而产生偏差。

在因果 DAG 中，我们通过包括 $L$ 和 $Y$ 的一个或多个共同原因来表示这种潜在的偏差。在这里，我们做了一个最简单的假设，即在所有时间点上，一个单一的、基线的原因（或一组原因）会影响 $L$ 和 $Y$ 。

下面的 DAG 是一个完整 的 DAG ，也就是说，除了直接从每个治疗方法 $A$ 到结果 $Y$ 的箭头，所有可能的箭头都存在。

为简单起见，在下面的 DAG 中省略了 $Y_k$, $Y_{k+1}$ 可以被认为是随访结束时结果的累积概率（发病率）的指标。

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node (1) {L$_{k-1}$};
\node [right =of 1] (2) {A$_{k-1}$};
\node [right =of 2] (3) {L$_{k}$};
\node [right =of 3] (4) {A$_{k}$};
\node [right =of 4] (5) {Y$_{k+1}$};
\node [below =of 1] (6) {U};

\draw[Arrow, thick] (1.east) -- (2.west);
\draw[Arrow, thick] (1) to [out=25, in=160] (3);
\draw[Arrow, thick] (1) to [out=25, in=160] (4);
\draw[Arrow, thick] (1) to [out=25, in=160] (5);

\draw[Arrow, thick] (2.east) -- (3.west);
\draw[Arrow, thick] (2) to [out=25, in=160] (4);

\draw[Arrow, thick] (3.east) -- (4.west);
\draw[Arrow, thick] (3) to [out=25, in=160] (5);

\draw[Arrow, thick] (6.north) -- (1.south);
\draw[Arrow, thick] (6.east) -- (3.south);
\draw[Arrow, thick] (6.east) -- (5.south);
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817160403872.png)

### 4.4 在箭头上进行注释

当使用 DAG 示意偏见时，有时需要对箭头进行注释，以表明偏见的方向是积极的或消极的，可能会有助于记忆与理解。该代码通过在每个箭头的中点上方或下方放置一个新的文本节点来为 DAG 箭头添加标签或注释。

在下面的例子中，混杂因素 $L$ 与结果 $Y$ 呈负相关，与暴露 $A$ 呈正相关，如果在分析中不控制 $L$ ，则 $A$ 与 $Y$ 之间的估计关联会出现负（或下）的偏差。

```latex
\documentclass{article}
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}

\begin{document}
\tikzstyle{Arrow} = [
thick,
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
},
shorten >= 3pt, preaction = {decorate}
					]

\begin{tikzpicture}
\node  (2) {L};
\node [right =of 2] (3) {A};
\node [right =of 3] (4) {Y};

\draw[Arrow] (2.east) -- (3.west) node[midway, below]{$+$};
\draw[Arrow] (2) to [out=25, in=160]  node[midway, above] {$-$} (4);
\draw[Arrow] (3.east) --(4.west) node[midway, below] {$-$};
\end{tikzpicture}

\end{document}
```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/image-20210817160625074.png)

&emsp;

&emsp;

## 5. 结语

利用 **LaTex** 进行绘制有向无环图更多的地方还是得自己进行操作，限于文献篇幅这里不介绍过多的方法，完整版的 **PDF** 与源码在参考文献当中。或者直接访问本文码云仓库：

> <https://gitee.com/arlionn/latex>


&emsp;

## 6. 文章附件

- [完整版生成文档 -PDF-](<https://gitee.com/arlionn/latex/blob/master/DAG/tex%E6%96%87%E6%A1%A3/%E4%BD%BF%E7%94%A8LaTex%E7%BB%98%E5%88%B6%E6%9C%89%E5%90%91%E6%97%A0%E7%8E%AF%E5%9B%BE%20(DAG).pdf>)
- [完整版代码 -.tex](<https://gitee.com/arlionn/latex/blob/master/DAG/tex%E6%96%87%E6%A1%A3/%E4%BD%BF%E7%94%A8LaTex%E7%BB%98%E5%88%B6%E6%9C%89%E5%90%91%E6%97%A0%E7%8E%AF%E5%9B%BE%20(DAG).tex>)
- [Github 开源原文](https://github.com/arlionn/causalgraphs_latex)
- [Overleaf, Online LaTeX Editor](https://www.overleaf.com/login)

&emsp;

## 7. 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh latex beamer`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 


- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata+LaTex：绘制流程图](https://www.lianxh.cn/news/94f69493f819c.html)
- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [Stata：输出漂亮的LaTeX表格-T222](https://www.lianxh.cn/news/f302057b30bb5.html)
  - [Stata结果输出：Excel结果表变身LaTeX表格](https://www.lianxh.cn/news/77e2251d6fbed.html)
  - [Stata与LaTeX的完美结合](https://www.lianxh.cn/news/79a6fe14cfbe6.html)
  - [Stata结果输出：用esttab生成带组别名称的LaTeX回归表格](https://www.lianxh.cn/news/b535fda7f2dac.html)
- 专题：[Markdown](https://www.lianxh.cn/blogs/30.html)
  - [Markdown教程之LaTeX数学公式简介](https://www.lianxh.cn/news/845d7f5a2d977.html)
  - [Markdown常用LaTex数学公式](https://www.lianxh.cn/news/554f3e9c9f08d.html)
  - [Markdown中书写LaTeX数学公式简介](https://www.lianxh.cn/news/c061d6b77c6aa.html)
- 专题：[工具软件](https://www.lianxh.cn/blogs/23.html)
  - [连享会工具：Beamer幻灯片制作](https://www.lianxh.cn/news/4700422eee871.html)
  - [LaTeX小白入门：TeXLive安装及使用](https://www.lianxh.cn/news/bb253cd035ef4.html)
  - [Word,MathType与LaTeX公式](https://www.lianxh.cn/news/3c5572deab713.html)



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 相关课程

>### 免费公开课  

- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[Bilibili 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[Bilibili 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等) 




>### 最新课程-直播课   

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)**|   | 文本分析、机器学习、效率专题、生存分析等 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |

- Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp; 

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

>### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，400+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等

&emsp; 

> 连享会小程序：扫一扫，看推文，看视频……

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp; 

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

> &#x270F;  连享会-常见问题解答：  
> &#x2728;  <https://gitee.com/lianxh/Course/wikis>  

&emsp; 

> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)
