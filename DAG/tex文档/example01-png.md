##### 2.3.1.1 单点暴露的统计学DAG

- 有混杂因素的统计DAG

在下一个 DAG 中，我们增加了一个已知的混杂因素，$L$ --即一个与暴露和结果相关的变量，并且在时间上先于两者发生，因此它可以解释暴露和结果之间检测到的任何虚假（非因果）关联。$L$ 也可以代表一组与 $A$ 和 $Y$ 有类似关系的混杂变量。

```latex
\documentclass{article}
% 文章类型文档声明
\usepackage{tikz}
\usetikzlibrary{positioning, calc, shapes.geometric, shapes, shapes.multipart, arrows.meta, arrows, decorations.markings, external, trees}
% 导入主要所需宏包

\begin{document}
% 开始文档内容书写

\tikzstyle{Arrow} = [
thick, 
decoration={
	markings,
	mark=at position 1 with {
	\arrow[thick]{latex}
	}
}, 
shorten >= 3pt, preaction = {decorate}
					]
% 设置箭头格式类型

\begin{tikzpicture}
\node  (2) {L};
\node [right =of 2] (3) {A};
\node [right =of 3] (4) {Y};

\draw[Arrow] (2.east) -- (3.west);
\draw[Arrow] (2) to [out=25, in=160] (4); 
\end{tikzpicture}
% 正式画图

\end{document}
```

![image-20210817151355659](https://gitee.com/Shutter_Zor/pictures_cloud/raw/master/image-20210817151355659.png)