
把推文中的 tex 文档放到这里，每个例子一个独立的 .tex 文档

### &#x2B55;下载 LaTeX 套装
比较成熟的 **LaTex** 排版工具可以下载 **Texstudio** 加 **Texlive** 的组合，相关内容可以使用[清华大学开源软件镜像站](https://mirrors.tuna.tsinghua.edu.cn/) 进行下载与安装，速度快且安全。

### &#x2B55; 在线编译 LaTeX 文档
考虑到下载速度等外部因素的影响，本文也提供一种在线的 **LaTex** 编辑器使用方式，后续 **画图** 部分的实操将通过 **overleaf** 实现：[Overleaf, Online LaTeX Editor](https://www.overleaf.com/login) 。大家可以通过各种方式登录，并新建一个临时项目，从而完成简易的绘图工作。